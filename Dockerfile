FROM openjdk:8-jdk-alpine
VOLUME /tmp
# Add Maven dependencies (not shaded into the artifact; Docker-cached)
ARG JAR_FILE
ADD ${JAR_FILE} app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
